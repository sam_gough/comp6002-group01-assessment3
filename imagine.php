<?php include_once('functions/functions.php'); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Imagine</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <link rel="stylesheet" href="css/style.css" type="text/css" >
    </head>
     <body onload='showimagine("index")'>
        <header>
            <img class = "banner" src="images/top-banner.jpg" alt="Banner"></img>
            <img class = "banner-small" src="images/banner-small.jpg" alt="Banner"></img>
        </header>
        <nav class="navbar navbar-inverse">
        <div class="container-fluid">
           
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="index.php">Home </a></li>
                <li><a href="courses.php">Course Information</a></li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BCS Lab Information <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="googleapps.php">Google Apps</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="slack.php">Slack</a></li>
                    <li role="separator" class="divider"></li>
                    <li class="active"><a href="#">Microsoft Imagine<span class="sr-only">(current)</span></a></li>
                    <li role="separator" class="divider"></li>
                </ul>
                </li>
                <li><a href="pandora.php">Pandora Network</a></li>
                <li><a href="usefullinks.php">Useful Links</a></li>
                <li><a href="login.php">Login</a></li>
            </ul>
            </div>
        </div>
        </nav>
      
        <div class="container">
            <div class="row justify-content-center">
                <div id="imag" class="col-med-6">
                    
                </div>
            </div>
        </div>    
        
<script src="js/scripts.js"></script>
    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>