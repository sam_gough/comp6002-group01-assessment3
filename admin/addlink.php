<?php   include_once('../functions/functions.php'); 
        session_start();
        logout();
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Add Link</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <link rel="stylesheet" href="../css/style.css" type="text/css" >
    </head>
    <body>
    <?php 
        if( $_SESSION['login'] == TRUE )
        {
        ?>
        <header>
            <img class = "banner" src="../images/top-banner.jpg" alt="Banner"></img>
            <img class = "banner-small" src="../images/banner-small.jpg" alt="Banner"></img>
        </header>
        <nav class="navbar navbar-inverse">
        <div class="container-fluid">
           
            <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li ><a href="index.php">Home</a></li>
                <li><a href="courses.php">Course Information</a></li>
                <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BCS Lab Information <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="googleapps.php">Google Apps</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="slack.php">Slack</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="imagine.php">Microsoft Imagine</a></li>
                    <li role="separator" class="divider"></li>
                </ul>
                </li>
                <li><a href="pandora.php">Pandora Network</a></li>
                <li><a href="usefullinks.php">Useful Links</a></li>
            </ul>
            </div>
        </div>
        </nav>

           <div class="contact-section">
			<div class="container">
                <div class="row">
				<form method="POST">
					<div class="col-md-6 form-line">
			  			<div class="form-group">
			  				<label>Web Url - Enter the website URL</label>
					    	<input type="text" class="form-control" id="weburl" placeholder="moodle2.boppoly.ac.nz">
				  		</div>
				  		<div class="form-group">
					    	<label>Image URL - Enter the web URL or file location</label>
					    	<input type="text" class="form-control" id="imageurl" placeholder= "images/slack.png">
					  	</div>	
					  	<div class="form-group">
					    	<label>Description - Brief Description of the Website</label>
					    	<input type="text" class="form-control" id="desc" placeholder="Description goes here">
			  			</div>
                        <div>
                            <input type="submit" name="addForm" onclick="addLink()" value="Add Link">
                            <button type="button" name="cancel" onclick="window.location='usefullinks.php'" class="btn btn-default submit">Return</button>
			  			</div>
			  		</div>
                 </form>
                </div>
            </div>

             <?php  
        }
        else
        {
        ?>
        <div id="spam">
            <h1 id="access"><spam>X</spam> Access Denied - You Do Not Have Access To This Page</h1>
            <p id="accessp">Secure content is being accessed</p>
            <li><a href="../login.php"><p id="accessp">Go To Login</p></a></li>
            <li><a href="../index.php"><p id="accessp">Go To Home Page</p></a></li>
            <br/><br/>
        </div>
        <?php
        }
        ?>

    <script src="../js/scripts.js"></script>


    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>