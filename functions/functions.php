<?php 

//DB CONNECTION//
include_once('dbase.php');

function connection() {
    
    date_default_timezone_set('Pacific/Auckland');
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

//GET ALL INDEX CONTENT//
function get_all_index() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_home";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "content" => $row['CONTENT']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//GET ALL COURSES CONTENT//
function get_all_courses(){
    
    $db = connection();
    $sql = "SELECT * FROM tbl_courses";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "tab" => $row['TAB'],
            "content" => $row['CONTENT']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//GET ALL GOOGLEAPPS CONTENT//
function get_all_googleapps() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_googleapps";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "content" => $row['CONTENT'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//GET ALL IMAGINE CONTENT//
function get_all_imagine() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_imagine";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "content" => $row['CONTENT'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//GET ALL PANDORA CONTENT//
function get_all_pandora() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_pandora";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "content" => $row['CONTENT'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//GET ALL SLACK CONTENT//
function get_all_slack() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_slack";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "content" => $row['CONTENT'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//GET ALL USEFUL LINKS//
function get_all_usefullinks() {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_usefullinks";

    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "weburl" => $row['WEBURL'],
            "imageurl" => $row['IMAGEURL'],
            "content" => $row['CONTENT'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

///////////////////

//Function to show info on edit page//
function get_link_json_by_id($id) {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_usefullinks WHERE ID = $id";
    
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "url" => $row['WEBURL'],
            "imageurl" => $row['IMAGEURL'],
            "content" => $row['CONTENT'],
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//Edit Useful Links//
function displayID()
{
    $id = $_GET['id'];
    $array = json_decode(get_link_json_by_id($id), True);
    return $array[0]['id'];
}

function displayWebUrl()
{
    $id = $_GET['id'];
    $array = json_decode(get_link_json_by_id($id), True);
    return $array[0]['url'];
}

function displayImageUrl()
{
    $id = $_GET['id'];
    $array = json_decode(get_link_json_by_id($id), True);
    return $array[0]['imageurl'];
}

function displayDescription()
{
    $id = $_GET['id'];
    $array = json_decode(get_link_json_by_id($id), True);
    return $array[0]['content'];
}

function editUsefullinks($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $weburl = $db->real_escape_string($array[0]['Weburl']);
    $imageurl = $db->real_escape_string($array[0]['Imageurl']);
    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    //var_dump($id);

    $sql = "UPDATE tbl_usefullinks SET WEBURL='".$weburl."', IMAGEURL='".$imageurl."', CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

//////////ADD USEFUL LINKS//////////////
function addLinkUsingJSON($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $weburl = $db->real_escape_string($array[0]['Weburl']);
    $imageurl = $db->real_escape_string($array[0]['Imageurl']);
    $desc = $db->real_escape_string($array[0]['Desc']);


    $stmt = $db->prepare("INSERT INTO tbl_usefullinks (WEBURL, IMAGEURL, CONTENT) VALUES (?, ?, ?)");
    $stmt->bind_param("sss", $weburl, $imageurl, $desc);
    $stmt->execute();
    
    print $stmt->error; //to check errors

    $result = $stmt->affected_rows;

    $stmt->close();
    $db->close();

    if ($result > 0 ) {

        $output = 1;
        return $output;
        
    }
    else {

        $output = 0;
        return $output;
        
    }
}

/////////Delete Useful Link////////////

function deletelink_id($jsonString) {
    
    $array = json_decode($jsonString, TRUE);
    
    $db = connection();

    $id = $db->real_escape_string($array[0]['ID']);

    $stmt = $db->prepare("DELETE FROM tbl_usefullinks WHERE ID = ?");
    $stmt->bind_param("i", $id);
    $stmt->execute();

    $result = $stmt->affected_rows;

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

//////////Update Index Page Content/////////////
function editIndexPage($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    var_dump($id);

    $sql = "UPDATE tbl_home SET CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

///////////EDIT COURSE PAGES FUNCTION////////////////
function editCoursePage($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    //var_dump($id);

    $sql = "UPDATE tbl_courses SET CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

/////////////EDIT GAPPS PAGE////////////////
function editGappsPage($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    //var_dump($id);

    $sql = "UPDATE tbl_googleapps SET CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

/////////////EDIT SLACK PAGE////////////////
function editSlackPage($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    //var_dump($id);

    $sql = "UPDATE tbl_slack SET CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

/////////////EDIT IMAGINE PAGE////////////////
function editImaginePage($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    //var_dump($id);

    $sql = "UPDATE tbl_imagine SET CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}

/////////////EDIT PANDORA PAGE////////////////
function editPandora($jsonString) {
    
    $array = json_decode($jsonString, TRUE);

    $db = connection();

    $content = $db->real_escape_string($array[0]['Content']);
    $id = $db->real_escape_string($array[0]['ID']);

    //var_dump($id);

    $sql = "UPDATE tbl_pandora SET CONTENT='".$content."' WHERE ID = ".$id."";

    $result = $db->query($sql);

    if ($result > 0 ) 
    {
        return 1;
    }
    else 
    {
        return 0;
    }
}
///////////////////




//ADMIN LOGIN//
function loginAdmin() {
    //$_SESSION = array();
    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_admin WHERE NAME = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['NAME'],
                "pass" => $row['PASSWRD']
            );
        }

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            $_SESSION['login'] = FALSE;
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
            
        }
    }
}


/////////Logout Admin//////////////


function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        $_SESSION = array();

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        session_destroy();

        redirect("../index.php");
    }
}

//REDIRECT//
function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}


?>