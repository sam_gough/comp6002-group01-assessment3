CREATE DATABASE IF NOT EXISTS `toiohomai_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `toiohomai_db`;

DROP TABLE IF EXISTS `tbl_home`;
CREATE TABLE `tbl_home`(
`ID` int(10) NOT NULL AUTO_INCREMENT,
`CONTENT` text(10000) NOT NULL,
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_home`(`ID`,`CONTENT`) VALUES
(1, '<h1>Welcome to BCS.NET.NZ</h1>
                    <p>Toi Ohomai Institute of Technology Bachelor of Computing and Mathematical Science.
                    This Intranet is for students who are completing the Bachelor of Computer Science in Applied Science.
                    On this little mini-site you can find information specific to the Pandora labs that are used as part of your course.</p>
                   
                    <h2>General student offers</h2>
                    <p >As a student of Toi Ohomai Institute of Technology, you also gain the benefits the students gain from other courses.
                    Most of them are listed on the getconnected website, but here are the most common parts.</p></br>
                    </br>
                    <p>To access Moodle go here = > <a href="https://moodle2.boppoly.ac.nz">https://moodle2.boppoly.ac.nz</a></br>
                    To download Office 2016 as part of Office 365 go here => <a href="https://www.boppoly.ac.nz/go/office-365-students">https://www.boppoly.ac.nz/go/office-365-students</a></p></br>
                    </br>
                    <p id="indexpara3">Using the Student Wi-Fi</br>
                    To connect your device to the Student Wifi to this:</br>
                    <ul id="indexwifi" class="wifi">
                    <li>Select Toi Ohomai Wi-Fi in your Wifi Settings</li>
                    <li>Username = moodle-id@stu.boppoly.ac.nz</li>
                    <li>Password = your student id (unless you have changed it)</li>
                    </ul>
                    </br>
                    <p>Any Questions?
                    If you have any questions, ask any of the IT staff and we can help you out.
                    You can also post your questions in Slack in the #general channel and other students can help out as well!</p>');

DROP TABLE IF EXISTS `tbl_courses`;
CREATE TABLE `tbl_courses` (
`ID` int(10) NOT NULL AUTO_INCREMENT,
`TAB` varchar(20),
`CONTENT` text(10000) NOT NULL, 
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_courses`(`ID`,`TAB`,`CONTENT`) VALUES
(1,'DAC5' ,'<h3>Diploma in Applied Computing Level 5</h3>
                            <p>Want to get a job as a computer programmer, systems analyst, website or software developer? Technology is rapidly changing the way we live and New Zealand desperately needs more computer professionals.

                            Level 5 and Level 6 Diplomas offer the only local pathway towards the University of Waikato Bachelor of Science, specialising in Applied Computing.

                            Its a logical three year programme, all based in Tauranga, with the added flexibility of stopping along the way and collecting these level 5 or level 6 Diplomas. You also have the option of studying the courses part-time.

                            This Level 5 Diploma in Applied Computing develops core skills in IT infrastructure, software, multimedia, networks and programming to meet a wide range of business computing needs.

                            Content Details

                            To complete the qualification you need to complete 120 credits, 60 credits from the core courses and 60 credits from the optional courses. Each course is worth 15 credits unless otherwise indicated. You can credit all the courses listed below to the first year of the University of Waikato Bachelor of Science (BSc).</p>

                            <p>Core Courses</br>

                            COMP5002 Introductory Programming</br>
                            INFT5001 Professional Skills</br>
                            COMP5004 IT Infrastructure</br>
                            COMP5008 Software Packages</br>
                            </br>
                            Optional Courses</br>

                            COMP5007 Introduction to Multimedia</br>
                            MATH5003 Statistics for Science AND</br>
                            ELEL5005 Introduction to Electronics</br>
                            COMP5003 IT Essentials and Networking (30 credits)</br>
                            </br>
                            Key</br>
                             Compulsory for the degree pathway</p>'),

(2,'DAC6' ,'<h3>Diploma in Applied Computing Level 6</h3>
                            <p>This computing and IT course develops your career as an intermediate IT professional. The Level 6 Diploma includes website development, business and project management, database systems and advanced programming.
                            Complete just one more year after this diploma to gain the University of Waikato Bachelor of Science (BSc) majoring in Computer Science with a specialisation in Applied Computing, taught right here in Tauranga.
                            Students benefit from strong industry connections and get plenty of opportunities to put theory into practice, using their creative and analytical skills to develop portfolios of their work to present to future employers.</p>
                            </br>
                            <p>
                            Choose 120 credits from the 20 credit courses below</br>
                            </br>
                            20 Credit Courses</br>
                            COMP6001 GUI Programming (COMP5002)</br>
                            COMP6002 Internet and Web Development (COMP5002 and COMP5007)</br>
                            BMGT6003 Business Systems</br>
                            Choose from NZDipBus courses BMGT6001 Human Resource Management or BMGT6009 Applied Management</br>
                            INFT6001 Database Practice and Experience (COMP5002)</br>
                            INFT6002 Help Desk (INFT5001 and COMP5008)</br>
                            INFT6003 Hardware and Operating Systems (COMP5004)</br>
                            COMP6011 Data Communications - Services (COMP5004)</br>
                            BMGT6012 Project Management and QA (COMP5002, INFT5001, COMP5004 and COMP5008)</br>
                            COMP6008 Advanced Programming (COMP5002)</br>
                            </br>
                            Key</br>
                            () The number in brackets denotes prerequisites</br>
                            Compulsory for the degree pathway</p>'),

(3,'BCs' ,'<h3>Bachelor of Computer Science with Specialisation in Applied Computing</h3>
                            <p>There is no need to leave Tauranga to get your computing degree. Successfully complete the Diploma in Applied Computing (Level 5) and (Level 6) and gain full credit for the first and second year of this three year University of Waikato degree, delivered in Tauranga.
                            You will take courses on software systems, web application and development, how people and computers interact and how to ensure systems work well and are easy to use.
                            Throughout the degree you will continuously put into practice what you learn through hands-on, practical projects.</br>
                            </br>
                            Content Details<br>
                            You will need a total of 360 credits to graduate. This includes 120 credits from each of the Diploma in Applied Computing Level 5 and Diploma in Applied Computing Level 6.
                            Have a look on the University of Waikatos website or their Undergraduate Computing and Mathematical Science Handbook for additional information about the courses.
                            The following are the 20 credit courses you will study during the final year of this degree.</br>
                            </br>
                            COMP33A Web Applications Development</br>
                            COMP223A/B Information Discovery</br>
                            COMP315A/B Information Systems Development</br>
                            COMP325A Introduction to Human - Computer Interaction</br>
                            Plus one Toi Ohomai Institute of Technology Diploma in Applied Computing (Level 6) 20 credit course.<p>');
                        
                        

DROP TABLE IF EXISTS `tbl_googleapps`;
CREATE TABLE `tbl_googleapps` (
`ID` int(10) NOT NULL AUTO_INCREMENT,
`CONTENT` text(10000) NOT NULL,
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_googleapps`(`ID`,`CONTENT`) VALUES
(1, '<div class="col-med-6">
                    <h1>Setting up your Google Profile</h1>
                    <p>With chrome you can sign into your google account from the browser app and on the webpage. If you do it from the browser app, then it will create a link to your google profile and sign into your personal account and the school account at the same time.
                    Have a look at this gif to see how to sign into Chrome. (Click on it to make it bigger)</p>
                    <h2>Setting up your Google Apps Account</h2>
                    <p>At the beginning of each semester, all the passwords are reset to 12345678</br>
                    When you sign in  with Chrome you will need to know your student Id.</br>
                    </br>
                    Your Google Apps ID = student-id@bcs.net.nz</br>
                    Your password is 12345678 (You will be asked to change this after you accept the terms and conditions)</p>
                    </br>
                    <a href="https://d3vv6lp55qjaqc.cloudfront.net/items/1y0u2N1V3M1c1Y1d3X3j/2017-02-24_09-08-37-1.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=720ae9eb">
                    <img class="gif" src="https://d3vv6lp55qjaqc.cloudfront.net/items/1y0u2N1V3M1c1Y1d3X3j/2017-02-24_09-08-37-1.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=720ae9eb" alt="google app gif" height="250px"></img>
                    </a>
                </div>');

DROP TABLE IF EXISTS `tbl_imagine`;
CREATE TABLE `tbl_imagine` (
`ID` int(10) NOT NULL AUTO_INCREMENT,
`CONTENT` text(10000) NOT NULL,
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_imagine`(`ID`,`content`) VALUES
(1, '<h1>What is Microsoft Imagine</h1>
                    <p>With the beginning of each semester we set the students up with a Microsoft Imagine Account.
                    This allows you to download some free products from Microsoft that you would otherwise have to pay for.
                    Once you have graduated or are no longer with us, you can keep the software and licenses, but you will not be able to access the website anymore.</p>
                    <h2>Setting up your Microsoft Imagine Account</h2>
                    <p>In your bcs.net.nz email (it is a gmail account) you should have an email from Toi Ohomai and the subject should say:</br>
                    "Your school created a Microsoft Imagine WebStore account for you"</br>
                    In the videos below you can see how to setup your account.</p></br>
                    </br>
                    <a href="https://d3vv6lp55qjaqc.cloudfront.net/items/3h212n3O2X1B0M0i2Z1i/Setup-MSImagine.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=20d5f031">
                        <img class="gif" src="https://d3vv6lp55qjaqc.cloudfront.net/items/3h212n3O2X1B0M0i2Z1i/Setup-MSImagine.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=20d5f031" alt="google app gif" height="250px"></img>
                    </a>

                    <h2>Download Software from Microsoft Imagine</h2>
                    </br>
                    <a href="https://d3vv6lp55qjaqc.cloudfront.net/items/0I2i2R0T1h3s3n2K2h3W/Download-From-MSImagine.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=824b06f5">
                        <img class="gif" src="https://d3vv6lp55qjaqc.cloudfront.net/items/0I2i2R0T1h3s3n2K2h3W/Download-From-MSImagine.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=824b06f5" alt="google app gif" height="250px"></img>
                    </a>');

DROP TABLE IF EXISTS `tbl_pandora`;
CREATE TABLE `tbl_pandora` (
`ID` int(10) NOT NULL AUTO_INCREMENT,
`CONTENT` text(10000) NOT NULL,
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_pandora`(`ID`,`CONTENT`) VALUES
(1, '<h3>Your Files</h3>
                    <p>The corporate network (the library and classrooms other than the 3rd floor) is completely separate from the Pandora network (DT219, DT303, DT308 and DT312).
                    Both networks have an network drive for you to store you files in and both networks refer to that drive as H-Drive, however they are different and do not relate to each other in anyway.
                    By using your Google Apps account you are able to access your files from any machine using a web browser and you will not be limited by this.</p>
                    <h3>Toi Ohomai Wi-Fi Network</h3>
                    <p>The student Wi-Fi is also completely separate from the Pandora Network. If you are using your own device or connect your mobile device, the easiest way to access your files is by using Google Apps
                    The Wifi network basically allows you to browse the web. There are some exceptions, but you will hear about that in the Web Development papers you will do in your degree.</p>
                    <h3>Can I access anything on the Pandora Network from Home?</h3>
                    <p>Simple answer - No you cannot.</br>
                    However your course material is on Moodle and you can put files on Google drive and you are able to access what you need from there.
                    In terms of software, everything we use in the labs is available for free either through MS Imagine or a student service or the program is actually free to download.</p>
                </div>');

DROP TABLE IF EXISTS `tbl_slack`;
CREATE TABLE `tbl_slack` (
`ID` int(10) NOT NULL AUTO_INCREMENT,
`CONTENT` text(10000) NOT NULL,
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_slack`(`ID`,`CONTENT`) VALUES
(1, '<h1>Setting up your Slack Account</h1>
                    <p>Within the IT Team and some of your courses we use slack to communicate.
                    To create your account simply go to https://to-bcs.slack.com and sign in with your Google Apps Account
                    Click on the gif below to make it bigger</p>
                    <h2>Slack apps for your device</h2>
                    <p>Slack has a (mobile) app for every platform basically.
                    Simply go to https://slack.com/downloads and it will detect what kind of system you are using.
                    If you are on an iOS or Android device, you look up slack in the App Store or Google Play Store.</p></br>
                    </br>
                    <a href="https://d3vv6lp55qjaqc.cloudfront.net/items/3L0k2E1m0U0A2j0z0g2L/Slack-Sign-into-slack.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=a215465d">
                    <img class="gif" src="https://d3vv6lp55qjaqc.cloudfront.net/items/3L0k2E1m0U0A2j0z0g2L/Slack-Sign-into-slack.gif?X-CloudApp-Visitor-Id=230cbed997248c4a171a56bd1d8d9c5c&v=a215465d" alt="google app gif" height="250px"></img>
                    </a>');

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  `CATEGORY` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'admin', 'password', '');

DROP TABLE IF EXISTS `tbl_usefullinks`;
CREATE TABLE `tbl_usefullinks`(
`ID` int(10) NOT NULL AUTO_INCREMENT,
`WEBURL` varchar(200) NOT NULL,
`IMAGEURL` varchar(200) NOT NULL,
`CONTENT` text(100) NOT NULL,
PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_usefullinks`(`ID`,`WEBURL`,`IMAGEURL`,`CONTENT`) VALUES
(1,'https://bitbucket.org/','images/bitbucketlink.jpg', 'Website used to store work in repositories'),
(2,'http://moodle2.boppoly.ac.nz/','images/moodlelink.jpg', 'View course information'),
(3,'https://to-bcs.slack.com','images/slacklink.jpg', 'Communicate with staff and other students'),
(4,'https://imagine.microsoft.com/en-us','images/imaginelink.jpg','Website to download free software'),
(5,'https://www.google.com/drive/','images/googledrivelink.jpg','Used to store course work'),
(6,'https://www.visualstudio.com/','images/visualstudiolink.jpg','Visual Studio used for programming courses'),
(7,'https://toiohomai.ac.nz/','images/toiohomailink.jpg','Link to the Toi-Ohomai Website'),
(8,'https://code.visualstudio.com/','images/codelink.png','Visual Studio Code for multimedia courses');