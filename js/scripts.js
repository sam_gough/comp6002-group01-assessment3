//////////////SHOW INDEX PAGE////////////////
function showindex(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showindex.php";
    }
    else
    {
        phplink = "remote/showindex.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
        var result = JSON.parse(this.responseText);
        console.log(result);

        for(var i = 0; i < result.length; i++)
        {
            if (page === "admin")
            {
                var a = document.getElementById("adminhome");
                a.value = result[i].content;

                
            }
            else
            {
                var a = document.getElementById("homepage");
                a.innerHTML += "<div>"+result[i].content+"<div>";
            }
        }
        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

//////////////SHOW COURSES PAGE//////////////////

function showcourses(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showcourses.php";
    }
    else
    {
        phplink = "remote/showcourses.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
        var result = JSON.parse(this.responseText);
        //console.log(result);

        var output="<ul id=\"content\" class=\"nav nav-tabs\">";
        var menucontent = "";

        for(var i = 0; i < result.length; i++)
        {
            if (page === "admin")
            {
               if(result[i].id == 1)
                {
                    output += '<li class="active"><a data-toggle="tab" href="#tab'+result[i].id+'">'+result[i].tab+'</a></li>';
                    menucontent += "<div  id=\"tab"+result[i].id+"\"  class=\"tab-pane fade in active\"><form method='POST' height='100'><textarea id='coursecont' value="+result[i].content+"></textarea><input type='submit' name='update' onclick='updateCourse("+result[i].id+")' value='Update'></div>";
                }
                else
                {
                    output += '<li><a data-toggle="tab" href="#tab'+result[i].id+'">'+result[i].tab+'</a></li>';
                    menucontent += "<div  id=\"tab"+result[i].id+"\"  class=\"tab-pane fade in \"><form method='POST' height='100'><textarea id='coursecont' value="+result[i].content+"></textarea><input type='submit' name='update' onclick='updateCourse("+result[i].id+")' value='Update'></div>";
                }

                
            }
            else
            {
                if(result[i].id == 1)
                {
                    output += '<li class="active"><a data-toggle="tab" href="#tab'+result[i].id+'">'+result[i].tab+'</a></li>';
                    menucontent += "<div  id=\"tab"+result[i].id+"\"  class=\"tab-pane fade in active\">"+result[i].content+"</div>";
                }
                else
                {
                    output += '<li><a data-toggle="tab" href="#tab'+result[i].id+'">'+result[i].tab+'</a></li>';
                    menucontent += "<div  id=\"tab"+result[i].id+"\"  class=\"tab-pane fade in \">"+result[i].content+"</div>";
                }
            }

            //var content = document.getElementById('content');
            //content.innerHTML = output+"<ul>";
            
            document.getElementById('menucontainer').innerHTML = output+"</ul>"+"<div class=\"tab-content clearfix\">"+menucontent+"</div>";
        }

        

        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

//////////////SHOW GOOGLE APPS PAGE/////////////////

function showgoogleapps(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showgoogleapps.php";
    }
    else
    {
        phplink = "remote/showgoogleapps.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
        var result = JSON.parse(this.responseText);
        console.log(result);

        for(var i = 0; i < result.length; i++)
        {
            if (page === "admin")
            {
                var a = document.getElementById("gappsadmin");
                a.value = result[i].content;

                
            }
            else
            {
                var a = document.getElementById("gapps");
                a.innerHTML += "<div>"+result[i].content+"<div>";
            }
        }
        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

/////////////SHOW IMAGINE PAGE////////////////

function showimagine(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showimagine.php";
    }
    else
    {
        phplink = "remote/showimagine.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
        var result = JSON.parse(this.responseText);
        console.log(result);

        for(var i = 0; i < result.length; i++)
        {
            if (page === "admin")
            {
                var a = document.getElementById("imagineadmin");
                a.value = result[i].content;

                
            }
            else
            {
                var a = document.getElementById("imag");
                a.innerHTML += "<div>"+result[i].content+"<div>";
            }
        }
        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

////////////SHOW PANDORA PAGE/////////////

function showpandora(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showpandora.php";
    }
    else
    {
        phplink = "remote/showpandora.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
        var result = JSON.parse(this.responseText);
        console.log(result);

        for(var i = 0; i < result.length; i++)
        {
            if (page === "admin")
            {
                var a = document.getElementById("pandoraadmin");
                a.value = result[i].content;

                
            }
            else
            {
                var a = document.getElementById("Pan");
                a.innerHTML += "<div>"+result[i].content+"<div>";
            }
        }
        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

//////////////////SHOW SLACK PAGE/////////////

function showslack(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showslack.php";
    }
    else
    {
        phplink = "remote/showslack.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = JSON.parse(this.responseText);
            console.log(result);

            for(var i = 0; i < result.length; i++)
            {
                if (page === "admin")
                {
                    //var a = document.getElementById("slackadmin");
                    //a.innerHTML += "<div>"+result[i].content+"<div>"
                    var a = document.getElementById("slackadmin");
                    a.value = result[i].content;

                    
                }
                else
                {
                    var a = document.getElementById("slack");
                    a.innerHTML += "<div>"+result[i].content+"<div>";
                }
            }
        }
    
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

/////////SHOW USEFUL LINKS FUNCTION/////////

function showusefullinks(page) 
{
    console.log(page);
  
    var phplink = "";
    if(page === "admin")
    {
        phplink = "../remote/showusefullinks.php";
    }
    else
    {
        phplink = "remote/showusefullinks.php";
    }

    console.log(phplink);

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() 
    {
        console.log(xhttp.readyState);
        console.log(xhttp.status);

        if (this.readyState == 4 && this.status == 200)
        {
        var result = JSON.parse(this.responseText);
        console.log(result);

        for(var i = 0; i < result.length; i++)
        {
            if (page === "admin")
            {
                var a = document.getElementById("adminlinks");
                 a.innerHTML += "<div class='col-md-4 text-center'><div class='panel panel-success panel-pricing'><div class='panel-heading'><img class='linkimg' src=../"+result[i].imageurl+"></div><div class='panel-body text-center'><p><strong></strong></p></div><ul class='list-group text-center'><li class='list-group-item'>"+result[i].content+"</li></ul><div class='panel-footer'><a class='btn btn-lg btn-block btn-success' onclick='deleteLink("+result[i].id+")'>Delete Entry</a><a class='btn btn-lg btn-block btn-success' href='editlink.php?id="+result[i].id+"'>Edit Entry</a></div></div></div>";

                
            }
            else
            {
                var a = document.getElementById("links");
                a.innerHTML += "<div class='col-md-4 text-center'><div class='panel panel-success panel-pricing'><div class='panel-heading'><img class='linkimg' src="+result[i].imageurl+"></div><div class='panel-body text-center'><p><strong></strong></p></div><ul class='list-group text-center'><li class='list-group-item'>"+result[i].content+"</li></ul><div class='panel-footer'><a class='btn btn-lg btn-block btn-success' href='+result[i].weburl+'>Go To Page</a></div></div></div>";
            }
        }
        }
    };
    
    xhttp.open("GET", phplink, true);
    xhttp.send();
}

/////// EDIT USEFULLINKS FUNCTION /////////


function editLinks() {
  
    var _weburl = document.getElementById('weburl').value;
    var _imageurl = document.getElementById('imageurl').value;
    var _content = document.getElementById('content').value;
    var _id = document.getElementById('id').value;

    var jsonraw = [{ ID: _id, Weburl:_weburl, Imageurl:_imageurl, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editlinks.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            console.log(result);
    

            if (result == 1)
            {
                
                setTimeout(3);
                document.location = "../admin/usefullinks.php";
                alert("Update Success");
                //console.log("Update Success");
                  
            }
            else
            {
                alert("Update Failure");
                
            }
        }
    };

    xhttp.send(json);
}

//////////ADD LINK FUNCTION///////////////
function addLink() {
  
    var _weburl = document.getElementById('weburl').value;
    var _imageurl = document.getElementById('imageurl').value;
    var _desc = document.getElementById('desc').value;

    var jsonraw = [{ Weburl:_weburl, Imageurl:_imageurl, Desc:_desc}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/addlink.php";
   

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);
        var result = this.responseText;
            console.log(result);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log("RESULT - "+result);
            

            if (result == 1)
            {
                setTimeout(3);
                document.location = "../admin/usefullinks.php";
                alert("Update Success");
            }
            else
            {
                alert("Update Failure");
            }
        }
    };

    xhttp.send(json);
}

//////////DELETE LINK FUNCTION/////////////
function deleteLink(id) {
  
    var _id = id;
    console.log(_id);
    var jsonraw = [{ ID: _id}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/deletelink.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            console.log(result);
            if (result == 1)
            {
                console.log("SUCCESS");
                window.location.reload(true);
            }
            else
            {
                console.log("NOPE!");
            }
        }
    };

    xhttp.send(json);
}

//////////EDIT INDEX CONTENT FUNCTION///////////
function updateIndex() {
  
    var _content = document.getElementById('adminhome').value;
    var _id = 1;

    var jsonraw = [{ ID: _id, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editindex.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log(result);

            if (result == 1)
            {
                window.location.reload(true);
                alert("Update Successful");
                  
            }
            else
            {
                alert("Update Failed");
                
            }
        }
    };

    xhttp.send(json);
}

////////////UPDATE COURSE PAGE///////////////
function updateCourse(id) {
  
    var _content = document.getElementById('coursecont').value;
    var _id = id;

    var jsonraw = [{ ID: _id, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editcourse.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log(result);

            if (result == 1)
            {
                window.location.reload(true);
                alert("Update Success");
                  
            }
            else
            {
                alert("Update Failed");
                
            }
        }
    };

    xhttp.send(json);
}

//////////EDIT GOOGLEAPPS CONTENT FUNCTION///////////
function updateGapps() {
  
    var _content = document.getElementById('gappsadmin').value;
    var _id = 1;

    var jsonraw = [{ ID: _id, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editgapps.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log(result);

            if (result == 1)
            {
                window.location.reload(true);
                alert("Update Successful");
                  
            }
            else
            {
                alert("Update Failed");
                
            }
        }
    };

    xhttp.send(json);
}

//////////EDIT SLACK CONTENT FUNCTION///////////
function updateSlack() {
  
    var _content = document.getElementById('slackadmin').value;
    var _id = 1;

    var jsonraw = [{ ID: _id, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editslack.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log(result);

            if (result == 1)
            {
                window.location.reload(true);
                alert("Update Successful");
                  
            }
            else
            {
                alert("Update Failed");
                
            }
        }
    };

    xhttp.send(json);
}

//////////EDIT IMAGINE CONTENT FUNCTION///////////
function updateImagine() {
  
    var _content = document.getElementById('imagineadmin').value;
    var _id = 1;

    var jsonraw = [{ ID: _id, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editimagine.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log(result);

            if (result == 1)
            {
                window.location.reload(true);
                alert("Update Successful");
                  
            }
            else
            {
                alert("Update Failed");
                
            }
        }
    };

    xhttp.send(json);
}

//////////EDIT PANDORA CONTENT FUNCTION///////////
function updatePandora() {
  
    var _content = document.getElementById('pandoraadmin').value;
    var _id = 1;

    var jsonraw = [{ ID: _id, Content:_content}];
    var json = JSON.stringify(jsonraw);
    var links = "../remote/editpandora.php";

    console.log("JSON - "+json);

    var xhttp = XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
    xhttp.open("POST", links, true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function() 
    {
        console.log("STATE - "+this.readyState);
        console.log("STATUS - "+this.status);

        if (this.readyState == 4 && this.status == 200)
        {
            var result = this.responseText;
            //console.log(result);

            if (result == 1)
            {
                window.location.reload(true);
                alert("Update Successful");
                  
            }
            else
            {
                alert("Update Failed");
                
            }
        }
    };

    xhttp.send(json);
}