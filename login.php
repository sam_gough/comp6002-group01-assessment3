<?php include_once('functions/functions.php');
session_start();
 loginAdmin();
  ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        
        <link rel="stylesheet" href="css/style.css" type="text/css" >
    </head>
    <body>
        <?php 
        if( !isset($_SESSION['login']) )
        {
        ?>
    <div id="fullscreen_bg" class="fullscreen_bg"/>

        <div class="container">
        	<form class="form-signin" method='POST'>
        		<h1 class="form-signin-heading text-muted">Sign In</h1>
		        <input type="username" class="form-control" name="username" placeholder="admin" required="" autofocus="">
		        <input type="password" class="form-control" name="password" placeholder="Password" required="">
		        <button class="btn btn-lg btn-primary btn-block" name="login" type="submit">
			    Sign In
		        </button>
	        </form>
        </div>
        <?php  
        }
        else
        {
            redirect("admin/index.php");
        }
        ?>

    <script   src="https://code.jquery.com/jquery-3.1.1.slim.min.js"   integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="   crossorigin="anonymous"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
</html>